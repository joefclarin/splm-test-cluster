/**
 * Gruntfile
 *
 * This Node script is executed when you run `grunt` or `sails lift`.
 * It's purpose is to load the Grunt tasks in your project's `tasks`
 * folder, and allow you to add and remove tasks as you see fit.
 * For more information on how this works, check out the `README.md`
 * file that was generated in your `tasks` folder.
 *
 * WARNING:
 * Unless you know what you're doing, you shouldn't change this file.
 * Check out the `tasks` directory instead.
 */

module.exports = function(grunt) {

    var cssFilesToInject = [
        '**/*.css'
    ];
    
    var jsFilesToInject = [
        'js/jquery/jquery-1.11.2.min.js',
        'js/underscore/underscore-min.js',
        'js/backbone/backbone-min.js',
        'js/backgrid/backgrid.min.js',
        'js/app.js',
        '**/*.js'
    ];
    
    var templateFilesToInject = [
        '**/*.html'
    ];
    
    cssFilesToInject = cssFilesToInject.map(function(path) {
        return '.tmp/public/' + path;
    });
    
    jsFilesToInject = jsFilesToInject.map(function(path) {
        return '.tmp/public/' + path;
    });
    
    templateFilesToInject = templateFilesToInject.map(function(path) {
        return 'assets/' + path;
    });
    
    var depsPath = grunt.option('gdsrc') || 'node_modules/sails/node_modules';
    grunt.loadTasks(depsPath + '/grunt-contrib-clean/tasks');
    grunt.loadTasks(depsPath + '/grunt-contrib-copy/tasks');
    grunt.loadTasks(depsPath + '/grunt-contrib-concat/tasks');
    grunt.loadTasks(depsPath + '/grunt-sails-linker/tasks');
    grunt.loadTasks(depsPath + '/grunt-contrib-jst/tasks');
    grunt.loadTasks(depsPath + '/grunt-contrib-watch/tasks');
    grunt.loadTasks(depsPath + '/grunt-contrib-uglify/tasks');
    grunt.loadTasks(depsPath + '/grunt-contrib-cssmin/tasks');

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-hashres');
    
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        
        copy: {
            prod: {
                files: [
                    {
                        expand: true,
                        cwd: './assets',
                        src: ['**/*.!(coffee)'],
                        dest: '.tmp/public'
                    }
                ]
            },
            build: {
                files: [
                    {
                        expand: true,
                        cwd: '.tmp/public',
                        src: ['**/*'],
                        dest: 'www'
                    }
                ]
            }
        },
        
        clean: {
            prod: ['.tmp/public/**'],
            build: ['www']
        },
        
        jshint: {
            all: ['Gruntfile.js', 'assets/js/pbook/**/*.js']
        },
        
        concat: {
            // js: {
                // src: jsFilesToInject,
                // dest: '.tmp/public/concat/production.js',
                // options: {
                    // separator: ";\n"
                // }
            // },
            css: {
                src: cssFilesToInject,
                dest: '.tmp/public/concat/production.css'
            }
        },
        
        jst: {
            prod: {

                // To use other sorts of templates, specify the regexp below:
                // options: {
                //   templateSettings: {
                //     interpolate: /\{\{(.+?)\}\}/g
                //   }
                // },

                files: {
                    '.tmp/public/jst.js': templateFilesToInject
                }
            }
        },
        
        uglify: {
            dist: {
                src: ['.tmp/public/concat/production.js'],
                dest: '.tmp/public/min/production.js'
            }
        },
        
        cssmin: {
            dist: {
                src: ['.tmp/public/concat/production.css'],
                dest: '.tmp/public/min/production.css'
            }
        },
        
        'sails-linker': {
            
            devJs: {
                options: {
                    startTag: '<!--SCRIPTS-->',
                    endTag: '<!--SCRIPTS END-->',
                    fileTmpl: '<script src="%s"></script>',
                    appRoot: '.tmp/public'
                },
                files: {
                    '.tmp/public/**/*.html': jsFilesToInject,
                    'views/**/*.html': jsFilesToInject,
                    'views/**/*.ejs': jsFilesToInject
                }
            },

            prodJs: {
                options: {
                    startTag: '<!--SCRIPTS-->',
                    endTag: '<!--SCRIPTS END-->',
                    fileTmpl: '<script src="%s"></script>',
                    appRoot: '.tmp/public'
                },
                files: {
                    '.tmp/public/**/*.html': ['.tmp/public/min/production.js'],
                    'views/**/*.html': ['.tmp/public/min/production.js'],
                    'views/**/*.ejs': ['.tmp/public/min/production.js']
                }
            },
            
            prodStyles: {
                options: {
                    startTag: '<!--STYLES-->',
                    endTag: '<!--STYLES END-->',
                    fileTmpl: '<link rel="stylesheet" href="%s">',
                    appRoot: '.tmp/public'
                },
                files: {
                    '.tmp/public/index.html': ['.tmp/public/min/production.css'],
                    'views/**/*.html': ['.tmp/public/min/production.css'],
                    'views/**/*.ejs': ['.tmp/public/min/production.css']
                }
            },
            
            devTpl: {
                options: {
                    startTag: '<!--TEMPLATES-->',
                    endTag: '<!--TEMPLATES END-->',
                    fileTmpl: '<script type="text/javascript" src="%s"></script>',
                    appRoot: '.tmp/public'
                },
                files: {
                    '.tmp/public/index.html': ['.tmp/public/jst.js'],
                    'views/**/*.html': ['.tmp/public/jst.js'],
                    'views/**/*.ejs': ['.tmp/public/jst.js']
                }
            }
        },
        
        hashres: {
            options: {
                encoding: 'utf8',
                fileNameFormat: "${name}.${hash}.${ext}",
                renameFiles: true
            },
            prod: {
                src: [
                        '.tmp/public/*.js', 
                        '.tmp/public/linker/**/*.js',
                        '.tmp/public/linker/**/*.css',
                        '.tmp/public/min/*.js',
                        '.tmp/public/min/*.css',
                        '.tmp/public/splunkjs/*.js',
                        '.tmp/public/splunkjs/**/*.js',
                        '.tmp/public/splunkjs/**/*.css'
                     ],
                dest: [
                        '.tmp/public/**/*.html',
                        'views/*.html',
                        'views/*.ejs',
                        'views/**/*.html',
                        'views/**/*.ejs'
                      ]
            }
        },
        
        watch: {
            api: {

                // API files to watch:
                files: ['api/**/*']
            },
            assets: {

                // Assets to watch:
                files: ['assets/**/*'],

                // When assets are changed:
                tasks: ['compileAssets', 'linkAssets']
            }
        }
    });
    
    grunt.registerTask('default', [
        'compileAssets',
        'linkAssets',
        'watch'
    ]);
    
    grunt.registerTask('compileAssets', [
        'clean:prod',
        'jst:prod',
        'copy:prod',
        'concat',
        //'uglify',
        'cssmin'
    ]);
    
    grunt.registerTask('linkAssets', [
        // Update link/script/template references in `assets` index.html
        //'sails-linker:prodJs',
        'sails-linker:devJs',
        'sails-linker:prodStyles',
        'sails-linker:devTpl',
        'hashres:prod'
    ]);
    
    grunt.registerTask('build', [
        'compileAssets',
        'linkAssets',
        'clean:build',
        'copy:build'
    ]);
};
