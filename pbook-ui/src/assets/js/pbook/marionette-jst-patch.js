Backbone.Marionette.Renderer.render = function(template, data) {
    template = "assets/templates/" + template;
    if (!JST[template]) throw "Template '" + template + "' not found!";
    console.debug("JST " + template + " found");
    return JST[template](data);
};