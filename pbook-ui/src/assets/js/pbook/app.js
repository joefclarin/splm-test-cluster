Pbook = {
    App: new Backbone.Marionette.Application(),
    Models: {},
    Collections: {},
    Views: {},
    ViewModel: {},
    Data: {},
    Routers: {},
    Consts: {},
    Utils: {}
};

Pbook.App.addRegions({
    mainRegion: "#main-container"
});

var PbookController = Backbone.Marionette.Controller.extend({
    displayPbook: function(){
        
    }
});

var PbookRouter = Backbone.Marionette.AppRouter.extend({
    controller: PbookController,
    appRoutes: {
        "": "displayPbook"
    }
});

Pbook.App.on("start", function() {
    if (Backbone.history) {
        Backbone.history.start();
    }
    var staticView = new Pbook.App.Phonebook();
    Pbook.App.mainRegion.show(staticView);
});

Pbook.App.addInitializer(function() {
    var pbookController = new PbookController();
    var pbookRouter = new PbookRouter({controller:pbookController});
});

$(document).ready(function() {
    Pbook.App.start();
});