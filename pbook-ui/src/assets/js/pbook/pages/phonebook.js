Pbook.App.Phonebook = Marionette.ItemView.extend({
    template: "phonebook.html",
    
    ui: {
        searchContainer: ".search-container",
        entriesContainer: ".entries-container"
    },
    
    events: {
        "click .search-button" : "clickSearchButton"
    },
    
    initialize: function() {
        console.log(this);
        this.getEntriesData("");
    },
    
    onRender: function() {
        // var data = new Backbone.Collection([
            // {"firstName": "a", "lastName": "aa", "id": "1", "phone": "09171", "emailAddress": "a.aa@nokia.com"},
            // {"firstName": "b", "lastName": "bb", "id": "2", "phone": "09172", "emailAddress": "b.bb@nokia.com"},
            // {"firstName": "c", "lastName": "cc", "id": "3", "phone": "09173", "emailAddress": "c.cc@nokia.com"}
        // ]);
        
        var data = this.data;
        
        var grid = new Backgrid.Grid({
            columns: [
                {name: "firstName", label: "First Name", cell: "string", editable: false, sortable: true},
                {name: "lastName", label: "Last Name", cell: "string", editable: false, sortable: true},
                {name: "id", label: "ID Number", cell: "string", editable: false, sortable: true},
                {name: "number", label: "Number", cell: "string", editable: false, sortable: true},
                {name: "emailAddress", label: "Email Address", cell: "string", editable: false, sortable: true}
            ],
            collection: data
        });
        
        this.ui.entriesContainer.html(grid.render().el);
    },
    
    clickSearchButton: function() {
        this.getEntriesData(this.getSearchFilter());
    },
    
    getSearchFilter: function() {
        return this.ui.searchContainer.find('.search-box').val();
    },
    
    getEntriesData: function(filter) {
        var url = 'pbook/api/entries' + (filter != "" ? "?filter=" + filter : "");
        
        Pbook.App.Entry = Backbone.Model.extend({});
        Pbook.App.Entries = Backbone.Collection.extend({
            model: Pbook.App.Entry,
            url: url
        });
        this.data = new Pbook.App.Entries();
        this.data.fetch();
        console.log(this.data);
    }
    
});