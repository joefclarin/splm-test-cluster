/**
 * RestApiController
 *
 * @description :: Server-side logic for managing restapis
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var request = require('request'),
    _ = require('underscore'),
    properties = require ("properties");
 
var pbook_backend_ip = '10.133.146.146';
var pbook_backend_port = '8443';
// var pbook_backend_ip = '10.133.1.248';
// var pbook_backend_port = '8080';
var cache = {};
 
function makeBackendURL(req) {
    return 'http://' + pbook_backend_ip + ":" + pbook_backend_port + req.url;
}
 
module.exports = {
	/**
     * Overrides for the settings in `config/controllers.js`
     * (specific to RestApiController)
     */
    _config: {},

    proxy: function(req, res) {
        var url = module.exports.checkCache(req, res);

        if (url) {
            request({
                uri: makeBackendURL(req),
                method: req.method,
                json: req.body
            },function(error, incomingMsg, body) {
                if (error) {
                    console.log(error);
                }
                module.exports.saveCache(url, body);
            }).pipe(res);
        }
    },
    
    checkCache: function(req, res) {
        console.log("Proxying " + req.method + " -> " + req.url);
        var url = makeBackendURL(req);

        if (sails.config.environment == 'development' && cache[url]) {
            console.log("From cache");
            res.send(cache[url]);
            return null;
        } else {
            return url;
        }
    },

    saveCache: function(url, response) {
        if (sails.config.environment == 'development') {
            cache[url] = response;
        }
    }
};
