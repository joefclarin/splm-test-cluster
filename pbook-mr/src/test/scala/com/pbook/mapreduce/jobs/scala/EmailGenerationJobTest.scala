/**
 *
 */
package com.pbook.mapreduce.jobs.scala

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.apache.hadoop.io.LongWritable
import org.scalatest.mock.MockitoSugar
import org.apache.hadoop.mapreduce.Mapper
import org.mockito.Mockito.verify
import org.apache.hadoop.io.Text
import java.util.Arrays

@RunWith(classOf[JUnitRunner])
class EmailGenerationJobTest extends FlatSpec with ShouldMatchers with MockitoSugar {

    val mapper = new EmailGenerationMapper
    val validLineFirstNameWithoutSpace = "Joy,Clarin,201,+639173068848"
    val validLineFirstNameWithSpaces = "Cromwell Justine,Ellamil,80,+639173068574"
    val validLineLastNameWithSpaces = "Justin,de los Reyes,999,+639173068575"
    
  val mapperContext = mock[mapper.Context]
  val dummyLongWritable = mock[LongWritable]
    
    "Mapper" should "create email with format firstname.lastname@pbook.com" in {
        //when
        mapper.map(dummyLongWritable, new Text(validLineFirstNameWithoutSpace), mapperContext)
        //then
        verify(mapperContext).write(new Text("Joy,Clarin,201,+639173068848,joy.clarin@pbook.com"), new Text)
    }

    "Mapper" should "create email with format firstname.lastname@pbook.com, get first string in first name" in {
        //when
        mapper.map(dummyLongWritable, new Text(validLineFirstNameWithSpaces), mapperContext)
        //then
        verify(mapperContext).write(new Text("Cromwell Justine,Ellamil,80,+639173068574,cromwell.ellamil@pbook.com"), new Text)
    }

    "Mapper" should "create email using first name and last name with format firstname.lastname@pbook.com, join last name with underscores" in {
        //when
        mapper.map(dummyLongWritable, new Text(validLineLastNameWithSpaces), mapperContext)
        //then
        verify(mapperContext).write(new Text("Justin,de los Reyes,999,+639173068575,justin.de_los_reyes@pbook.com"), new Text)
    }  

    val reducer = new EmailGenerationReducer
    val reducerContext = mock[reducer.Context]

    "Reducer" should "output same line as input" in {
        reducer.reduce(new Text("Justin,de los Reyes,999,+639173068575,justin.de_los_reyes@pbook.com"), Arrays.asList(), reducerContext)
        verify(reducerContext).write(new Text("Justin,de los Reyes,999,+639173068575,justin.de_los_reyes@pbook.com"), new Text)
    }
}