package com.pbook.mapreduce.formatter;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.util.List;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper.Context;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author James.Necio
 */
@RunWith(MockitoJUnitRunner.class)
public class FormatNumberMapperTest {
        FormatNumberMapper testObj;
        
        ArgumentCaptor<Object> keyCaptor = ArgumentCaptor.forClass(Object.class);
        ArgumentCaptor<Object> valueCaptor = ArgumentCaptor.forClass(Object.class);
        
        @SuppressWarnings("rawtypes")
        @Mock
        private Context context;
        
        Text[] inputTexts = new Text[] {
                        new Text("Joefrenette Joy	Clarin	201	09173068848"),
                        new Text("Cromwell Justine	Ellamil	80	09173068574"),
                        new Text("James Carl	Necio	166	09173068704")
                };
        
        Text[] inputTextsRegular = new Text[] {
                        new Text("Joefrenette Joy	Clarin	201	+639173068848"),
                        new Text("Cromwell Justine	Ellamil	80	+639173068574"),
                        new Text("James Carl	Necio	166	+639173068704")
                };
        
        Text[] inputTextsWithDeformed = new Text[] {
                        new Text("Joefrenette Joy	Clarin	201	+639173068848"),
                        new Text("Cromwell Justine Ellamil	80	+639173068574"),
                        new Text("James Carl|Necio|166|+639173068704")
                };
        
        List<Text> outputTexts = ImmutableList.of(
                new Text("Joefrenette Joy,Clarin,201,+639173068848"),
                new Text("Cromwell Justine,Ellamil,80,+639173068574"),
                new Text("James Carl,Necio,166,+639173068704")
        );
        
        @Before
        public void setup() {
                 testObj = new FormatNumberMapper();
        }
        
        @Test
        public void shouldBeAbleToFormatRegularRow() throws Exception {
                assertFormattedRows(inputTexts, 3);
        }
        
        @Test
        public void shouldBeAbleToFormatRowsWithRegularRow() throws Exception {
                assertFormattedRows(inputTextsRegular, 3);
        }
        
        @Test
        public void shouldBeAbleToFormatRowsWithDeformedRow() throws Exception {
                assertFormattedRows(inputTextsWithDeformed, 1);
        }

        private void assertFormattedRows(Text[] inputTexts, int count) throws InterruptedException, IOException {
                final LongWritable longWritable = new LongWritable();
                
                //when
                for (Text inputText : inputTexts) {
                        testObj.map(longWritable, inputText, context);
                }
                
                //then
                verify(context, times(count)).write(keyCaptor.capture(), valueCaptor.capture());
                for (Object outputText : valueCaptor.getAllValues()) {
                        assertTrue(outputTexts.indexOf(outputText) != -1);
                }
        }
}
