package com.pbook.mapreduce.formatter;

import java.util.Arrays;
import java.util.HashSet;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer.Context;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author James.Necio
 */
@RunWith(MockitoJUnitRunner.class)
public class FormatNumberReducerTest {
        FormatNumberReducer testObj;
        
        ArgumentCaptor<Object> keyCaptor = ArgumentCaptor.forClass(Object.class);
        ArgumentCaptor<Object> valueCaptor = ArgumentCaptor.forClass(Object.class);
        
        @SuppressWarnings("rawtypes")
        @Mock
        private Context context;
        
        @Before
        public void setup() {
                testObj = new FormatNumberReducer();
        }
        
        @Test
        public void should_reduce() throws Exception {
                Text key = new Text("James Carl,Necio,166,+639173068704");
                Iterable<Text> values = Arrays.asList(
                        new Text("James Carl,Necio,166,+639173068704"),
                        new Text("James Carl,Necio,166,+639173068704"));
                ArgumentCaptor<Text> captor = ArgumentCaptor.forClass(Text.class);
                
                //when
                testObj.reduce(key, values, context);
                
                //then
                verify(context).write(eq(NullWritable.get()), captor.capture());
                assertThat(captor.getAllValues().size(), equalTo(1));
                assertThat(new HashSet<Object>(captor.getAllValues()), equalTo(new HashSet<Object>(Arrays.asList(
                        new Text("James Carl,Necio,166,+639173068704")
                ))));
        }
}
