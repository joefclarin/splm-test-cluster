package com.pbook.mapreduce.jobs;

import com.pbook.mapreduce.formatter.FormatNumberMapper;
import com.pbook.mapreduce.formatter.FormatNumberReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 *
 * @author James.Necio
 */
public class FormatNumberJob extends Configured implements Tool {
	public static final String SPLIT_SIZE = "64000000";
        
        public static void main(String args[]) throws Exception {
                ToolRunner.run(new FormatNumberJob(), args);
        }
        
        @Override
        public int run(String[] args) throws Exception {
                setDefaultConfig();

		String input = args[0];
		String output = args[1];
                
		Configuration config = getConf();
                Job job = new Job(config, "Format numbers");
                
                job.setMapperClass(FormatNumberMapper.class);
                job.setReducerClass(FormatNumberReducer.class);
                
                job.setMapOutputKeyClass(Text.class);
                job.setMapOutputValueClass(Text.class);
                
                job.setOutputKeyClass(NullWritable.class);
                job.setOutputValueClass(Text.class);
                
		FileInputFormat.setInputPaths(job, new Path(input));
		FileOutputFormat.setOutputPath(job, new Path(output));

                return job.waitForCompletion(true) ? 0 : 1;
        }

	private void setDefaultConfig() {
		Configuration config = getConf();
		config.setIfUnset("io.sort.mb", "128");
		config.setIfUnset("mapred.max.split.size", SPLIT_SIZE);
	}
}
