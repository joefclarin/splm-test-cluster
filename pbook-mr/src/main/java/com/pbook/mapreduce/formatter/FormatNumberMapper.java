package com.pbook.mapreduce.formatter;

import com.google.common.base.Joiner;
import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author James.Necio
 */
public class FormatNumberMapper extends Mapper<LongWritable, Text, Text, Text> {

        @Override
        protected void map(LongWritable key, Text row, Context context) throws IOException, InterruptedException {
                String[] columns = row.toString().split("\t");
                if(columns.length == 4) {
                        Joiner joiner = Joiner.on(",");
                        columns[3] = columns[3].replaceAll("^09", "+639");
                        String formattedRow = joiner.join(columns);
                        context.write(new Text(formattedRow), new Text(formattedRow));
                }
        }
}
