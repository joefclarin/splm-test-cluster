package com.pbook.mapreduce.formatter;

import java.io.IOException;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

/**
 *
 * @author James.Necio
 */
public class FormatNumberReducer extends Reducer<Text, Text, NullWritable, Text> {
        private MultipleOutputs<NullWritable, Text> mos;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
                super.setup(context);
                mos = new MultipleOutputs<NullWritable, Text>(context);
        }
        
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
                context.write(NullWritable.get(), key);
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
                mos.close();
        }
}
