/**
 *
 */
package com.pbook.mapreduce.jobs.scala

import org.apache.hadoop.conf.Configured
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.Mapper
import org.apache.hadoop.mapreduce.Reducer
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.util.Tool

/**
 * @author lukaszjastrzebski
 *
 */
trait CommonJobUtils extends Configured with Tool {

  val SPLIT_SIZE = "64000000"

  protected def setDefaultConfig() {
    val config = getConf()
    config.setIfUnset("io.sort.mb", "128")
    config.setIfUnset("mapred.max.split.size", SPLIT_SIZE)
  }

  protected def deleteOutputPath(outPath: Path) {
    val dfs = FileSystem.get(outPath.toUri(), getConf())
    if (dfs.exists(outPath)) {
      dfs.delete(outPath, true)
    }
  }

  protected def createJob(name: String, mapper: Class[_ <: Mapper[_, _, _, _]],
    reducer: Class[_ <: Reducer[_, _, _, _]]) = {
    val config = getConf()
    val job = new Job(config, name)
    job.setJarByClass(classOf[CommonJobUtils])

    job.setMapperClass(mapper)
    job.setReducerClass(reducer)
    job
  }

  protected def prepareRunner(input: String, output: String, args: Array[String]): Job

  override def run(args: Array[String]): Int = {
    setDefaultConfig();

    val input = args(0);
    val output = args(1);
    val job = prepareRunner(input, output, args)

    FileInputFormat.setInputPaths(job, new Path(input))
    val outPath = new Path(output)
    FileOutputFormat.setOutputPath(job, outPath)

    deleteOutputPath(outPath)
    if (job.waitForCompletion(true)) 0 else 1
  }
}