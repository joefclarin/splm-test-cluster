package com.pbook.mapreduce.jobs.scala

import org.apache.hadoop.util.ToolRunner
import org.apache.hadoop.util.Tool
import org.apache.hadoop.mapreduce.Mapper
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Reducer
import org.apache.hadoop.io.NullWritable
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.mapreduce.Job
import com.google.common.collect.Lists

class EmailGenerationMapper extends Mapper[LongWritable, Text, Text, Text] {

   override protected[scala] def map(key: LongWritable, value: Text,
    context: Mapper[LongWritable, Text, Text, Text]#Context): Unit = {
        val splitValues = value.toString().split(",")
        if (splitValues.length == 4) {
            val firstName = splitValues(0).toLowerCase().split(" ")(0);
            val lastName = splitValues(1).toLowerCase().split(" ").mkString("_");
            val email = firstName + "." + lastName + "@pbook.com"
            
            context.write(new Text(List(value, email).mkString(",")), new Text)
        }
    }
}

class EmailGenerationReducer extends Reducer[Text, Text, Text, Text] {
  override protected[scala] def reduce(key: Text, values: java.lang.Iterable[Text], 
      context: Reducer[Text, Text, Text, Text]#Context): Unit = {
    context.write(key, new Text)
  }
}

class EmailGenerationRunner extends CommonJobUtils with Tool {

  override protected def prepareRunner(input: String, output: String, args: Array[String]): Job = {
    val job = createJob("ultimate header",
      classOf[EmailGenerationMapper],
      classOf[EmailGenerationReducer])
    job.setMapOutputKeyClass(classOf[Text])
    job.setMapOutputValueClass(classOf[Text])

    job.setOutputKeyClass(classOf[NullWritable])
    job.setOutputValueClass(classOf[Text])
    job
  }
}

object EmailGenerationJob extends App {
  ToolRunner.run(new EmailGenerationRunner, args);
}