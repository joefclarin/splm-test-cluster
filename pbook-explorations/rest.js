var http = require('http');

function run_cmd(cmd, args, callBack ) {
    var spawn = require('child_process').spawn;
    var child = spawn(cmd, args);
    child.stdout.on('data', function (buffer) { callBack('[out] ' + buffer.toString()); });
    child.stderr.on('data', function (buffer) { callBack('[err] ' + buffer.toString()); });
} 

var port = process.argv[1] | 8080;
http.createServer(function (req, res) {
  var body = "";
  req.on('data', function (chunk) {
    body += chunk;
  });
  req.on('end', function () {
    var jobReq = JSON.parse(body);
    var date = new Date();
    console.log('[' + date + '] Request: ' + JSON.stringify(jobReq));
    var runType = jobReq['runType'] || 'default';
    if(Object.keys(methods).indexOf(runType) === -1)
      runType = 'default';
    console.log("Running method: " + runType);
    methods[runType](jobReq);
  });
  res.writeHead(201, {'Content-Type': 'text/plain'});
  res.end('');
}).listen(port);

var date1 = new Date();
console.log('[' + date1 + '] ' + 'starting up rest server on port: ' + port);

var methods = {
  "default": function(jobReq) {
    run_cmd('Rscript', ['remove_duplications.R', jobReq['inputTableName']],
      function(text) {
          var date1 = new Date();
          console.log ('[' + date1 + '] ' + text);
      }
    );
  }
};