pbook.hive.init <- function(hive_host, port, cluster_size = 12, sort_mem="256", xmx="2G") {
  Sys.setenv(HIVE_HOME="/usr/lib/hive/")
  Sys.setenv(RHIVE_DATA="/mnt/srv/rhive_data")
  Sys.setenv(HADOOP_HOME="/usr/lib/hadoop/")
  Sys.setenv(RHIVE_FS_HOME="/RHive")
  require('RHive')
  require('rJava')
  rhive.init()
  rhive.env()
  rhive.connect(host = hive_host, port = port, hiveServer2=TRUE)	
  rhive.set("io.sort.mb", sort_mem)
  rhive.set("mapred.child.java.opts", paste("-Xmx", xmx, sep=""))
  rhive.set("mapred.max.split.size", as.character(floor(320000000 / cluster_size)))
  rhive.set("hive.exec.reducers.max", as.character(10 * cluster_size))
  rhive.set("hive.exec.reducers.bytes.per.reducer", "10383360")
  rhive.set("hive.exec.max.dynamic.partitions.pernode", "50000");
  rhive.set("hive.exec.max.dynamic.partitions", "200000");
  rhive.set("hive.exec.dynamic.partition.mode", "nonstrict");
  rhive.set("hive.exec.dynamic.partition", "true");
}