#!/usr/bin/env python

import sys

current_key = None

# input comes from STDIN
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # parse the input we got from mapper.py
    # word, count = line.split('\t', 1)
    key, entry = line.split('\t', 1)

    if current_key != key:
      current_key = key
      print '%s' % (entry)

# do not forget to output the last word if needed!
if current_key != key:
    print '%s' % (entry)