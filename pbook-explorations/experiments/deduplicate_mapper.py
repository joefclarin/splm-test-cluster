#!/usr/bin/python

import sys

for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    columns = line.split(',')
    key = columns[0]
    print '%s\t%s' % (key, line)

