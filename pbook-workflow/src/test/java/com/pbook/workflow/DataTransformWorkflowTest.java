/**
 * 
 */
package com.pbook.workflow;

import com.google.common.collect.ImmutableMap;
import com.pbook.workflow.api.RunMRJobTask;
import com.pbook.workflow.service.HDFS;
import java.util.List;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author lukaszjastrzebski
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class DataTransformWorkflowTest {

	private DataTransformWorkflow testObj;

	@Mock
	private RunMRJobTask mrJob;

	@Mock
	private HDFS hdfs;

	@Before
	public void setup() {
		testObj = new DataTransformWorkflow(mrJob, hdfs);
	}

	@Test
	public void should_execute_all_steps() throws Exception {
		when(hdfs.generateTmpDirName(anyString())).thenReturn("format_number", "generate_email");
		// when
		String output = testObj.execute("input");
		// then
		ArgumentCaptor<String[]> captor = ArgumentCaptor
				.forClass(String[].class);
		verify(mrJob, times(2)).execute(captor.capture());
		List<String[]> values = captor.getAllValues();
		assertThat(values.get(0), arrayContaining("com.pbook.mapreduce.jobs.FormatNumberJob", "input", "format_number"));
		assertThat(values.get(1), arrayContaining("com.pbook.mapreduce.jobs.scala.EmailGeneration", "format_number", "generate_email"));
                assertEquals(output, "generate_email");
	}

}
