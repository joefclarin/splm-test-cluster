/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pbook.workflow.api;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Justin.Delos_Reyes
 */
@RunWith(MockitoJUnitRunner.class)
public class ImportToHdfsTest {
    
	private ImportToHdfs testObj;
	@Mock
	private MoveFromLocalToHDFS moveFromLocalToHDFS;
	
	@Before
	public void setup() {
		testObj = new ImportToHdfs(moveFromLocalToHDFS);
	}
	@SuppressWarnings("unchecked")
	@Test
	public void should_extract_and_import() throws Exception {
		when(moveFromLocalToHDFS.execute(Matchers.anyList())).thenReturn("x2");
		//when
		String hdfsImportDir = testObj.execute(Arrays.asList("/dummy/path/to/file.tsv"));
		//then
		verify(moveFromLocalToHDFS).execute(Arrays.asList("/dummy/path/to/file.tsv"));
		assertThat(hdfsImportDir, equalTo("x2"));
	}
    
}
