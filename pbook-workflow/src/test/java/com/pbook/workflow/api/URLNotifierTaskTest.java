package com.pbook.workflow.api;

import com.pbook.workflow.model.ExperimenterTrigger;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Cromwell.Ellamil
 */
@RunWith(MockitoJUnitRunner.class)
public class URLNotifierTaskTest {
    
    private URLNotifierTask testObj;
    
    @Mock
    HttpClient client;
    
    @Mock
    HttpResponse response;
    
    @Captor
    ArgumentCaptor<HttpPost> httpCaptor;
    
    private static final String TABLE_NAME = "pbook_entries";
    private static final String URL = "http://10.133.145.250:9990";
    
    @Before
    public void setUp() {
        testObj = new URLNotifierTask();
        testObj = Mockito.spy(testObj);
        Mockito.doReturn(client).when(testObj).create();
    }

    /**
     * Test of execute method, of class URLNotifierTask.
     * @throws java.lang.Exception
     */
    @Test
    public void should_kick_web_service() throws Exception {
        // Given
        ProtocolVersion version = new ProtocolVersion("HTTP", 1, 1);
        when(response.getStatusLine()).thenReturn(new BasicStatusLine(version , 201, "ok"));
        when(client.execute(any(HttpPost.class))).thenReturn(response);
        
        // When
        testObj.execute(URL, new ExperimenterTrigger(TABLE_NAME));
        
        // Then
        verify(client).execute(httpCaptor.capture());
        
        // When
        HttpPost post = httpCaptor.getValue();
        String contentType = post.getHeaders("Content-Type")[0].getValue();
        
        // Then
        assertThat(contentType, equalTo("application/json"));
        String value = EntityUtils.toString(post.getEntity());
        assertThat(value, containsString("\"inputTableName\":\"" + TABLE_NAME + "\""));
    }
}
