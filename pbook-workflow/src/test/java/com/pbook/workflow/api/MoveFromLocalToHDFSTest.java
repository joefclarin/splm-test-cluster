package com.pbook.workflow.api;

import com.pbook.workflow.service.HDFS;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import static org.hamcrest.Matchers.endsWith;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author balnagy
 */
@RunWith(MockitoJUnitRunner.class)
public class MoveFromLocalToHDFSTest {

    MoveFromLocalToHDFS testObj;

    @Mock
    private FileSystem mockFS;

    @Mock
    private HDFS mockHDFS;

    @Captor
    private ArgumentCaptor<Path[]> paths;

    @Captor
    private ArgumentCaptor<Path> path;

    public static final String SRC_DIR = "/tmp/vfnztmp123";

    public static final String IMPORT_TMP_DIR = "/user/splm/importtmp";

    @Before
    public void setup() {
       testObj = spy(new MoveFromLocalToHDFS(mockFS, mockHDFS));
    }

    @Test
    public void execute_moveFiles() throws IOException {
        // given
        //Collection<File> files = new ArrayList<File>();
        ArrayList<String> files = new ArrayList<>();
        File temp1 = File.createTempFile("tempfile1", ".csv"); 
        File temp2 = File.createTempFile("tempfile2", ".csv");
        //write it
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(temp1));
        bw1.write("This is the temporary file content 1");
        bw1.close();
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(temp2));
        bw2.write("This is the temporary file content 2");
        bw2.close();
        
        files.add(temp1.getAbsolutePath());
        files.add(temp2.getAbsolutePath());
        
        when(mockHDFS.generateTmpDir(anyString())).thenReturn("/user/splm/importtmp");

        // when
        String result = testObj.execute(files);

        // then
        verify(mockFS, times(1)).moveFromLocalFile(paths.capture(), path.capture());
        Path[] paths1 = paths.getValue();
        assertThat(paths1[0].toString(), endsWith(".csv"));
        assertThat(paths1[1].toString(), endsWith(".csv"));
        assertThat(result, endsWith(IMPORT_TMP_DIR));
    }
}
