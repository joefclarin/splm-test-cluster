package com.pbook.workflow.api;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Justin.Delos_Reyes
 */
@RunWith(MockitoJUnitRunner.class)
public class LoadDataToHiveTableTest {
    
    LoadDataToHiveTable testObj; 
    
    @Mock
    private JdbcTemplate hql;
    
    private String tableName = "dummy_table";
    private String mrOutputDir = "/user/tmp/dir/output";
    
    @Before
    public void setup() {
        testObj = new LoadDataToHiveTable(hql);
    }
    
    @Test
    public void should_create_table_and_load() {
        testObj.execute(tableName, mrOutputDir);
        
        verify(hql, times(1)).execute("CREATE TABLE IF NOT EXISTS dummy_table(FirstName STRING, LastName STRING, Id STRING, Number STRING, Email STRING)");
        verify(hql, times(1)).update("LOAD DATA INPATH '/user/tmp/dir/output' INTO TABLE dummy_table");
    }
    
}
