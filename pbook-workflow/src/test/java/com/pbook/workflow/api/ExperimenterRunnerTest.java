package com.pbook.workflow.api;

import com.pbook.workflow.PbookE2EWorkflow;
import com.pbook.workflow.model.ExperimenterTrigger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author Cromwell.Ellamil
 */
@RunWith(MockitoJUnitRunner.class)
public class ExperimenterRunnerTest {
    
    private ExperimenterRunner testObj;
    
    @Mock
    private URLNotifierTask urlKicker;
    
    @Captor
    private ArgumentCaptor<String> rStudioUrl;
    
    @Captor
    private ArgumentCaptor<ExperimenterTrigger> experimenterTrigger;
            
    private static final String TABLE_NAME = PbookE2EWorkflow.PBOOK_ENTRIES_TABLE_NAME;
    private static final String RSTUDIO_URL = "http://10.133.145.250:9990";

    @Before
    public void setUp() throws Exception {
        testObj = new ExperimenterRunner(urlKicker, RSTUDIO_URL);
    }
    
    /**
     * Test of execute method, of class ExperimenterRunner.
     * @throws java.lang.Exception
     */
    @Test
    public void should_trigger() throws Exception {
        // Given
        ExperimenterTrigger trigger = new ExperimenterTrigger(TABLE_NAME);
        
        // When
        testObj.execute(trigger);
        
        // Then
        verify(urlKicker, times(1)).execute(rStudioUrl.capture(), experimenterTrigger.capture());
        assertThat(rStudioUrl.getValue(), equalTo(RSTUDIO_URL));
        assertThat(experimenterTrigger.getValue(), equalTo(trigger));
    }
}
