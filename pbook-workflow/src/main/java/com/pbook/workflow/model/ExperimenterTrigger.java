package com.pbook.workflow.model;

import java.util.Objects;

/**
 *
 * @author Cromwell.Ellamil
 */
public class ExperimenterTrigger {
    
    private String inputTableName;
    private static final String EMPTY_STRING = "";

    public ExperimenterTrigger() {
        inputTableName = EMPTY_STRING;
    }

    public ExperimenterTrigger(String inputTableName) {
        this.inputTableName = inputTableName;
    }

    public String getInputTableName() {
        return inputTableName;
    }

    public void setInputTableName(String inputTableName) {
        this.inputTableName = inputTableName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.inputTableName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExperimenterTrigger other = (ExperimenterTrigger) obj;
        return true;
    }
    
}
