package com.pbook.workflow.helpers;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

import org.springframework.jdbc.datasource.SimpleDriverDataSource;

public class PrestoDataSource extends SimpleDriverDataSource {
	public PrestoDataSource() {
		super();
	}

	/**
	 * @param driver
	 * @param url
	 * @param conProps
	 */
	public PrestoDataSource(Driver driver, String url, Properties conProps) {
		super(driver, url, conProps);
	}

	/**
	 * @param driver
	 * @param url
	 * @param username
	 * @param password
	 */
	public PrestoDataSource(Driver driver, String url, String username,
			String password) {
		super(driver, url, username, password);
	}

	/**
	 * @param driver
	 * @param url
	 */
	public PrestoDataSource(Driver driver, String url) {
		super(driver, url);
	}

	/* (non-Javadoc)
	 * @see org.springframework.jdbc.datasource.SimpleDriverDataSource#getConnectionFromDriver(java.util.Properties)
	 */
	@Override
	protected Connection getConnectionFromDriver(Properties props)
			throws SQLException {
		Connection connection = super.getConnectionFromDriver(props);
		connection.setCatalog("hive");
		return connection;
	}
}
