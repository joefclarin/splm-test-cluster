/**
 * 
 */
package com.pbook.workflow.helpers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.compress.utils.IOUtils;

/**
 * @author anukaul
 * 
 */
public class FileExtraction {

	/**
	 * Untar an input file into an output file.
	 * 
	 * @param inputFile
	 *            the input .tar file
	 * @param outputDir
	 *            the output directory file.
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ArchiveException
	 * @return 0 if succeeds
	 * 
	 */

	public int unTar(File inputFile, File outputDir)
			throws FileNotFoundException, IOException, ArchiveException {

		TarArchiveInputStream myTarFile = new TarArchiveInputStream(
				new FileInputStream(inputFile));
		TarArchiveEntry entry = null;
		while ((entry = (TarArchiveEntry) myTarFile.getNextEntry()) != null) {

			String fileName = removeDotsFromFilePath(entry.getName());
			final File outputFile = new File(outputDir, fileName);
			if (entry.isDirectory()) {
				createDirectory(outputFile);
			} else {
				createDirectory(outputFile.getParentFile());
				final OutputStream outputFileStream = new FileOutputStream(
						outputFile);
				IOUtils.copy(myTarFile, outputFileStream);
				outputFileStream.close();
			}

		}
		myTarFile.close();

		return 0;
	}

	private void createDirectory(final File outputFile) {
		if (!outputFile.exists()) {
			if (!outputFile.mkdirs()) {
				throw new IllegalStateException(String.format(
						"Couldn't create directory %s.", outputFile
								.getParentFile().getAbsolutePath()));
			}
		}
	}

	/**
	 * Ungzip an input file into an output file.
	 * 
	 * @param inputFile
	 *            the input .tgz or .gz file
	 * @param outputDir
	 *            the output directory file.
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ArchiveException
	 * @return 0 if succeeds
	 * 
	 */
	public int unGzip(File inputFile, File outputDir)
			throws FileNotFoundException, IOException, ArchiveException {

		String fileType = "";
		fileType = getFileType(inputFile);

		if (fileType.equals("")) {
			System.out.println("NO EXTENSION ");
		}
		// Remove the extenion the file name .
		final File outputFile = new File(outputDir, inputFile.getName()
				.substring(0, inputFile.getName().length() - fileType.length()));

		// Read the input file
		final GZIPInputStream in = new GZIPInputStream(new FileInputStream(
				inputFile));

		final FileOutputStream out = new FileOutputStream(outputFile);

		IOUtils.copy(in, out);

		in.close();
		out.close();

		// After the Gzip we need to untar the file.
		unTar(outputFile, outputDir);

		// delete the file Gzipped file
		outputFile.delete();
		return 0;
	}

	/**
	 * Unzip an input file into an output file.
	 * 
	 * 
	 * @param inputFile
	 *            the input .zip file
	 * @param outputDir
	 *            the output directory file.
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws ArchiveException
	 * @return 0 if succeeds
	 * 
	 */
	public int unZip(File inputFile, File outputDir)
			throws FileNotFoundException, IOException, ArchiveException {

		ZipFile zipfile = new ZipFile(inputFile);
		ZipEntry zipEntry = null;
		Enumeration<? extends ZipEntry> zipEnum = zipfile.entries();

		while (zipEnum.hasMoreElements()) {
			zipEntry = (ZipEntry) zipEnum.nextElement();
			String fileName = removeDotsFromFilePath(zipEntry.getName());
			final File outputFile = new File(outputDir, fileName);

			if (zipEntry.isDirectory()) {
				createDirectory(zipfile, outputFile);
			} else {
				createDirectory(zipfile, outputFile.getParentFile());
				BufferedInputStream inputStream = new BufferedInputStream(
						zipfile.getInputStream(zipEntry));
				BufferedOutputStream outputStream = new BufferedOutputStream(
						new FileOutputStream(outputFile));
				IOUtils.copy(inputStream, outputStream);
				inputStream.close();
				outputStream.close();
			}

		}
		zipfile.close();
		return 0;
	}

	private void createDirectory(ZipFile zipfile, final File outputFile)
			throws IOException {
		if (!outputFile.exists()) {
			if (!outputFile.mkdirs()) {
				zipfile.close();
				throw new IllegalStateException(String.format(
						"Couldn't create directory %s.",
						outputFile.getAbsolutePath()));
			}
		}
	}

	/*
	 * Strip of the ../ or ./ from the file path "
	 */
	public String removeDotsFromFilePath(String filePath) {

		filePath = filePath.replace("../", "");
		filePath = filePath.replace("::", "_");
		return filePath.replace("./", "");

	}

	public String getFileType(File inputFile) {

		String fileType = "";

		int index = inputFile.getName().lastIndexOf(".");
		if (index > 0) {
			fileType = inputFile.getName().substring(index + 1);
		}
		return fileType;

	}

	public boolean isEmptyDirectory(File directory) {

		if (directory.listFiles().length <= 0) {
			return false;
		}
		return true;
	}

	/**
	 * @param inputPath
	 * @param outputFolder
	 * @throws CompressorException 
	 * @throws IOException 
	 * @throws ArchiveException 
	 */
	public void unBZip2(File inputPath, File outputFolder) throws CompressorException, IOException, ArchiveException {
		final InputStream is = new FileInputStream(inputPath);
		CompressorInputStream in = new CompressorStreamFactory()
				.createCompressorInputStream("bzip2", is);
		File tarFile = new File(outputFolder, "output.tar");
		IOUtils.copy(in, new FileOutputStream(tarFile));		
		in.close();

		unTar(tarFile, outputFolder);
		tarFile.delete();
	}

}
