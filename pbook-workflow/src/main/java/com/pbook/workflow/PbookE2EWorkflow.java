package com.pbook.workflow;

import com.pbook.workflow.api.ImportToHdfs;
import com.pbook.workflow.api.LoadDataToHiveTable;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.CompressorException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Justin.Delos_Reyes
 */
@Service
public class PbookE2EWorkflow {
    private static final org.slf4j.Logger LOG = LoggerFactory
			.getLogger(PbookE2EWorkflow.class);
    
    private ImportToHdfs importToHdfs;
    private LoadDataToHiveTable loadDataToHiveTable;
    private DataTransformWorkflow dataTransformWorkflow;
    
    public static final String PBOOK_ENTRIES_TABLE_NAME = "pbook_entries";
    
    @Autowired
    public PbookE2EWorkflow(ImportToHdfs importToHdfs, LoadDataToHiveTable loadDataToHiveTable,
            DataTransformWorkflow dataTransformWorkflow) {
        this.importToHdfs = importToHdfs;
        this.loadDataToHiveTable = loadDataToHiveTable;
        this.dataTransformWorkflow = dataTransformWorkflow;
    }
    
    public void execute(List<String> filepaths) {
        try {
            LOG.info("Enter Import to HDFS");
            String hdfsDir = this.importToHdfs.execute(filepaths);
            String mapReduceOutputDir = this.dataTransformWorkflow.execute(hdfsDir);
            
            this.loadDataToHiveTable.execute(PBOOK_ENTRIES_TABLE_NAME, mapReduceOutputDir);
            
            // call remove duplicates api in R
            // clean up temp dirs
            
            for(String filepath : filepaths) {
                File file = new File(new URI(filepath));
                if(!file.exists()) {
                    LOG.error("File doesn't exist");
                }
                if(!file.delete()) {
                    LOG.error("Could not delete {}", file);
                }
                LOG.info("Deleting {}", file);
            }
            
        } catch (IOException | ArchiveException | CompressorException ex) {
            Logger.getLogger(PbookE2EWorkflow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(PbookE2EWorkflow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PbookE2EWorkflow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
