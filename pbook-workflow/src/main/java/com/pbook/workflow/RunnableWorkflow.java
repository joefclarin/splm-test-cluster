/**
 * 
 */
package com.pbook.workflow;

/**
 * @author lukaszjastrzebski
 *
 */
public interface RunnableWorkflow {

	Integer execute(String[] args) throws Exception;
}
