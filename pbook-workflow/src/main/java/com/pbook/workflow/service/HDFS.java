package com.pbook.workflow.service;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author balnagy
 */
@Service
public class HDFS {

    @Value("${pbook.hdfs.importtmp}")
    private String tmpDirectory;

    private FileSystem fs;

    @Autowired
    public HDFS(FileSystem fs) {
        this.fs = fs;
    }
    
    public String generateTmpDirName(String prefix) throws IOException {
        String tmp = tmpDirectory + "/" + prefix + "_" + Long.toString(System.nanoTime());
        return tmp;
    }

    public String generateTmpDir(String prefix) throws IOException {
        String tmp = generateTmpDirName(prefix);
        fs.mkdirs(new Path(tmp));
        return tmp;
    }

    public void deletePath(String output) throws IOException {
    	if (output == null) {
    		return;
    	}
        Path outPath = new Path(output);
        if (fs.exists(outPath)) {
            fs.delete(outPath, true);
        }
    }
}
