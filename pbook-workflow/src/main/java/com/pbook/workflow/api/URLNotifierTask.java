package com.pbook.workflow.api;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class URLNotifierTask {
    
    private static final Logger LOG = LoggerFactory.getLogger(URLNotifierTask.class);
    
    public <T> void execute(String url, T trigger) throws IOException {
        LOG.info("About to create http client");
        HttpClient client = create();
        ObjectMapper mapper = new ObjectMapper();
        HttpPost post = new HttpPost(url);
        post.addHeader("Content-Type", "application/json");
        String jsonTrigger = mapper.writeValueAsString(trigger);
        HttpEntity entity = new StringEntity(jsonTrigger, "UTF-8");
        post.setEntity(entity);
        LOG.info("Try to kick {} with {}", url, jsonTrigger);
        HttpResponse response = client.execute(post);
        LOG.info("Kicked {} with {} result {}", url, jsonTrigger, response.getStatusLine());
    }
    
    public HttpClient create() {
        return new DefaultHttpClient();
    }
}
