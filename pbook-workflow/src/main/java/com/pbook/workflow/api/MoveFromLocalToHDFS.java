package com.pbook.workflow.api;

import com.pbook.workflow.service.HDFS;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author balnagy
 */
@Service
public class MoveFromLocalToHDFS {

    private static final Logger LOG = LoggerFactory.getLogger(MoveFromLocalToHDFS.class);

    private final FileSystem fs;
    private HDFS hdfs;

    @Autowired
    public MoveFromLocalToHDFS(FileSystem fs, HDFS hdfs) {
        this.fs = fs;
        this.hdfs = hdfs;
    }

    public String execute(List<String> filesToImport) throws IOException {
        String tmp = hdfs.generateTmpDir("raw_input");
        LOG.info("tempDir : {}", tmp);
        List<Path> srcPaths = new ArrayList<Path>();
        LOG.info("srcPaths : {}", srcPaths);
        for (String toImport : filesToImport) {
            File file = new File(toImport.replace("file://", ""));
            LOG.info("file to import : {}", file);
            if(file.length() > 0) {
                LOG.info("processed : {}", file.getAbsoluteFile());
                srcPaths.add(new Path(file.getAbsolutePath()));
            }
            LOG.info("file processed : {}, srcPaths  : {}", toImport, srcPaths);
        }
        LOG.info("final srcPaths : {}", srcPaths);
        Path[] srcPathsArray = new Path[filesToImport.size()];
        Path[] list = srcPaths.toArray(srcPathsArray);
        for(Path p : list) {
            LOG.info("srcPaths to array : {}", p);   
        }
        fs.moveFromLocalFile(srcPaths.toArray(srcPathsArray), new Path(tmp));
        return tmp;
    }

    protected Collection<File> listFiles(String srcDir) {
        return FileUtils.listFiles(new File(srcDir), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
    }

}
