package com.pbook.workflow.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author Justin.Delos_Reyes
 */
@Service
public class LoadDataToHiveTable {
    
    private static final Logger LOG = LoggerFactory.getLogger(LoadDataToHiveTable.class);
    
    private JdbcTemplate hql;
    
    @Autowired
    public LoadDataToHiveTable(@Qualifier("jdbcTemplate") JdbcTemplate hiveOperations) {
        this.hql = hiveOperations;
    }
    
    public void execute(String tableName, String mrOutputDir) {
        createTable(tableName);
        loadData(tableName, mrOutputDir);
    }
    
    private void createTable(String tableName) {
        LOG.info("Creating table : "  + tableName);
        // change EmployeeId to Id
        this.hql.execute("CREATE TABLE IF NOT EXISTS " + tableName + "(FirstName STRING, LastName STRING, Id STRING, Number STRING, Email STRING)");
    }
    
    private void loadData(String tableName, String mrOutputDir) {
        LOG.info("Loading to Hive the data from : " + mrOutputDir);
        this.hql.update("LOAD DATA INPATH '" + mrOutputDir + "' INTO TABLE " + tableName);
    }
}
