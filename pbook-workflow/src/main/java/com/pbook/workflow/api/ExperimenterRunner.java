package com.pbook.workflow.api;

import com.google.common.base.Preconditions;
import com.pbook.workflow.model.ExperimenterTrigger;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Cromwell.Ellamil
 */
@Service
public class ExperimenterRunner {
    
    private static final Logger LOG = LoggerFactory.getLogger(ExperimenterRunner.class);
    
    private final URLNotifierTask urlKicker;
    private final String rStudioUrl;

    @Autowired
    public ExperimenterRunner(URLNotifierTask urlKicker,
            @Value("${pbook.r_studio.url}") String rStudioUrl) {
        this.urlKicker = urlKicker;
        this.rStudioUrl = rStudioUrl;
    }
    
    public void execute(ExperimenterTrigger trigger) {
        Preconditions.checkArgument(trigger != null, "Trigger cannot be null.");
        Preconditions.checkArgument(trigger.getInputTableName() != null && !trigger.getInputTableName().isEmpty(), 
                "Table Name cannot be null nor empty.");
        
        try {
            LOG.info("Triggering experimenter");
            urlKicker.execute(rStudioUrl, trigger);
            LOG.info("Done Triggering experimenter");
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
