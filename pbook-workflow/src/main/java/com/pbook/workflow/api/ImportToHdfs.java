package com.pbook.workflow.api;

import java.io.IOException;
import java.util.List;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.CompressorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Justin.Delos_Reyes
 */
@Service
public class ImportToHdfs {
    
    private static final Logger LOG = LoggerFactory
			.getLogger(ImportToHdfs.class);
    
    private MoveFromLocalToHDFS moveFromLocalToHDFS;
    
    @Autowired
    public ImportToHdfs(MoveFromLocalToHDFS moveFromLocalToHDFS) {
		this.moveFromLocalToHDFS = moveFromLocalToHDFS;
    }
    
    public String execute(List<String> filepaths) throws IOException, ArchiveException, CompressorException {
        LOG.info("Import to HDFS started");
        LOG.info("Moving to hdfs..{}", filepaths);
        String hdfsImportDir = this.moveFromLocalToHDFS.execute(filepaths);
        return hdfsImportDir;
    }
}
