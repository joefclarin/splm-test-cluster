/**
 * 
 */
package com.pbook.workflow.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.hadoop.mapreduce.JarRunner;
import org.springframework.stereotype.Service;

import com.pbook.workflow.RunnableWorkflow;

/**
 * @author lukaszjastrzebski
 *
 */
@Service
public class RunMRJobTask implements RunnableWorkflow {

	private final JarRunner runner;

	@Autowired
	public RunMRJobTask(JarRunner runner) {
		this.runner = runner;
	}
	
	public synchronized Integer execute(String[] args) throws Exception {
		String[] newArray = new String[args.length - 1];
		System.arraycopy(args, 1, newArray, 0, newArray.length);
		this.runner.setArguments(newArray);
		this.runner.setMainClass(args[0]);
		return this.runner.call();
	}
}
