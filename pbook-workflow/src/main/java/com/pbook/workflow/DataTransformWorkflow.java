package com.pbook.workflow;

import com.pbook.workflow.api.RunMRJobTask;
import com.pbook.workflow.service.HDFS;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.hadoop.HadoopException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Justin.Delos_Reyes
 */
@Service
public class DataTransformWorkflow {
    
    private final RunMRJobTask mrJob;
    private final HDFS hdfs;
    
    @Autowired
    public DataTransformWorkflow(RunMRJobTask mrJob, HDFS hdfs) {
            super();
            this.mrJob = mrJob;
            this.hdfs = hdfs;
    }
    
    public String execute(String input) throws Exception {
        String tmpInput = hdfs.generateTmpDirName("format_number");
        String mrOutput = hdfs.generateTmpDirName("generate_email");
        try {
            int exitCode = mrJob.execute(new String[] {
                            "com.pbook.mapreduce.jobs.FormatNumberJob", input,
                            tmpInput });
            if (exitCode != 0) {
                    throw new HadoopException("FormatNumberJob failed");
            }
            exitCode = mrJob.execute(new String[] {
                            "com.pbook.mapreduce.jobs.scala.EmailGeneration",
                            tmpInput, mrOutput });
            if (exitCode != 0) {
                    throw new HadoopException("EmailGenerationJob failed");
            }
            return mrOutput;
        } finally {
                hdfs.deletePath(tmpInput);
        }
    }
}
