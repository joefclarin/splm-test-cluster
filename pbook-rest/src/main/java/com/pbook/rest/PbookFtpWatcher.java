package com.pbook.rest;

import com.pbook.rest.helpers.JobRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

/**
 *
 * @author Cromwell.Ellamil
 */
@Service
public class PbookFtpWatcher {
    
    public static final String TSV_FILE_REGEX = "^pbook.*.tsv";
    public static final String INBOX = "inbox";
    public static final String FTP_ROOT_JETTY = "/mnt/hadoop/ftp/";
    private static final Logger LOG = LoggerFactory.getLogger(PbookFtpWatcher.class);
    private static final Long FIVE_MINUTES = 300000L;
    
    private final List<String> fileUrls;
    private final PbookScheduler scheduler;
    private final String ftpRoot;
    
    private String ftpRootJetty;
    private final String ftpUser;
    private final String ftpPassword;
    private final TaskScheduler executor;
    
    @Autowired
    public PbookFtpWatcher(
            final PbookScheduler scheduler,
            TaskScheduler executor,
            @Value("${pbook.watcher.ftproot}") String ftpRoot,
            @Value("${pbook.watcher.user}") String ftpUser,
            @Value("${pbook.watcher.password}") String ftpPassword) throws IOException {
        
        LOG.info("PbookFtpWatcher started");
        this.scheduler = scheduler;
        this.executor = executor;
        this.ftpRoot = ftpRoot;
        this.ftpUser = ftpUser;
        this.ftpPassword = ftpPassword;
        this.fileUrls = new ArrayList<>();
        
        scheduleImport();
        LOG.info("Watching FTP inbox");
    }
    
    private void scheduleImport() {       
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    gatherAndImportFilesThenClearList();
                } catch (Exception e) {
                    LOG.error("Error in scheduling files", e);
                }
            }
        }, FIVE_MINUTES);
    }
    
    private void gatherAndImportFilesThenClearList() {
        final List<String> filesToImport = new ArrayList<>();
        
        checkTsvFiles();
        
        filesToImport.addAll(fileUrls);
        
        LOG.info("Processing files {}", filesToImport);
        if (filesToImport.size() > 0)
            this.scheduler.schedulePbookJob(JobRequest.aJobRequest(this.ftpUser, this.ftpPassword, filesToImport));
        
        this.clearFileList();
    }
    
    private void checkTsvFiles() {
        LOG.info("Checking inbox for new files.");
        try {
            Files.walkFileTree(new File(ftpRoot + INBOX).toPath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    buildFilesToProcess(file);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch(IOException e) {
            throw new RuntimeException("Unable to check existing files in the FTP inbox", e);
        }
        LOG.info("List of files gathered: {}", fileUrls);
    }
    
    private void buildFilesToProcess(Path path) {
        if (isValidPhonebookTsv(path)) {
            this.fileUrls.add(getAbsoluteImportPath(path));
        }
    }
    
    private boolean isValidPhonebookTsv(Path path) {
        return path.getFileName().toString().matches(TSV_FILE_REGEX);
    }
    
    public List<String> getFileUrls() {
        return this.fileUrls;
    }
    
    private String getAbsoluteImportPath(Path file) {
        String absolutePath = file.toFile().getAbsolutePath();
        return "file://" + absolutePath.replace(ftpRoot, FTP_ROOT_JETTY).replace("\\", "/");
    }
    
    public void clearFileList() {
        LOG.info("Clearing file list");
        this.fileUrls.clear();
    }
    
}
