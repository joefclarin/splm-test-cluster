package com.pbook.rest.fixtures;

import java.util.Random;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.bio.SocketConnector;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * @author lukaszjastrzebski
 * 
 */
public class JettyStarter {

	private Server server;
	private int port;

	/**
	 * @throws Exception
	 * 
	 */
	public void start() throws Exception {
		server = new Server();
		port = new Random().nextInt(200) + 12345;
		SocketConnector connector = new SocketConnector();
		connector.setSoLingerTime(-1);
		connector.setPort(port);
		server.addConnector(connector);

		WebAppContext bb = new WebAppContext();

		bb.setServer(server);
		bb.setContextPath("/");
		bb.setWar("src/test/webapp");

		server.setHandler(bb);

		server.start();
	}

	/**
	 * @throws Exception 
	 * 
	 */
	public void stop() throws Exception {
		server.stop();
	}

	/**
	 * @return
	 */
	public String getURL() {
		return "http://localhost:" + port;
	}

}
