package com.pbook.rest.handlers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Path("property")
public class TestApi {
        private String thisProperty;
        
        @Autowired
        public TestApi(@Value("${hive.host}") String thisProperty) {
                this.thisProperty = thisProperty;
        }
        
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Path("hive.host")
        public String getProperty() {
                return thisProperty;
        }
}
