package com.pbook.rest.handlers;

import com.pbook.rest.helpers.PhoneBookEntry;
import com.pbook.rest.helpers.PhoneBookRepository;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Cromwell.Ellamil
 */
@Component
@Path("entries")
public class PhoneBookApi {
    
    private final PhoneBookRepository phoneBookRepo;
    
    private static final Logger LOG = LoggerFactory.getLogger(PhoneBookApi.class);
    
    @Autowired
    public PhoneBookApi(PhoneBookRepository phoneBookRepo) {
        this.phoneBookRepo = phoneBookRepo;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public List<PhoneBookEntry> getPhoneBookEntries(@QueryParam("filter") final String filter) {
        List<PhoneBookEntry> phoneBookEntries = phoneBookRepo.getEntries(filter);
        if (phoneBookEntries == null) {
            return new ArrayList<>();
        }
        return phoneBookEntries;
    }
}
