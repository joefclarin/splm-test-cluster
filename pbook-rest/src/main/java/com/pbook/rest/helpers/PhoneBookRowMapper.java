package com.pbook.rest.helpers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Cromwell.Ellamil
 */
public class PhoneBookRowMapper implements RowMapper<PhoneBookEntry> {

    private static final String FIRST_NAME = "firstname";
    private static final String LAST_NAME = "lastname";
    private static final String ID = "id";
    private static final String NUMBER = "number";
    private static final String EMAIL_ADRESS = "email";
    
    @Override
    public PhoneBookEntry mapRow(ResultSet rs, int rowNum) throws SQLException {
        PhoneBookEntry entry = new PhoneBookEntry();
        
        entry.setFirstName(rs.getString(FIRST_NAME));
        entry.setLastName(rs.getString(LAST_NAME));
        entry.setId(rs.getString(ID));
        entry.setNumber(rs.getString(NUMBER));
        entry.setEmailAddress(rs.getString(EMAIL_ADRESS));
        return entry;
    }
    
}
