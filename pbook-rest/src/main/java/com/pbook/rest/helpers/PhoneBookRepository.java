package com.pbook.rest.helpers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PhoneBookRepository {
    
    private final JdbcTemplate hql;
    
    private final String TABLE_NAME = "pbook_entries_wod";
    
    // columns
    private final String FIRST_NAME = "firstname";
    private final String LAST_NAME  = "lastname";
    private final String ID         = "id";
    private final String NUMBER     = "number";
    private final String EMAIL      = "email";
    
    @Autowired
    public PhoneBookRepository(@Qualifier("prestoTemplate") JdbcTemplate hiveOperations) {
        this.hql = hiveOperations;
    }
    
    public List<PhoneBookEntry> getEntries() {
        return getEntries(null);
    }
    
    public List<PhoneBookEntry> getEntries(String filter) {
        String testQuery = createQuery(filter);
        List<PhoneBookEntry> result = hql.query(testQuery, new PhoneBookRowMapper());
        return result;
    }
    
    private String createQuery(String filter) {
        String query = "SELECT * FROM " + TABLE_NAME;
        
        if(filter != null && !filter.isEmpty()) {
            query += " WHERE ";
            query += FIRST_NAME + " = '" + filter + "' OR ";
            query += LAST_NAME  + " = '" + filter + "' OR ";
            query += ID         + " = '" + filter + "' OR ";
            query += NUMBER     + " = '" + filter + "' OR ";
            query += EMAIL      + " = '" + filter + "'";
        }
        
        return query;
    }
}
