package com.pbook.rest.helpers;

import java.util.List;

/**
 *
 * @author Cromwell.Ellamil
 */
public class JobRequest {
    
    private String username;
    private String password;
    private List<String> fileUrls;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getFileUrls() {
        return fileUrls;
    }

    public void setFileUrls(List<String> fileUrls) {
        this.fileUrls = fileUrls;
    }

    public static JobRequest aJobRequest(String username, String password, List<String> fileUrls) {
        JobRequest request = new JobRequest();
        
        request.username = username;
        request.password = password;
        request.fileUrls = fileUrls;
        return request;
    }
}
