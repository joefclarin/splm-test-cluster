package com.pbook.rest;

import com.pbook.rest.helpers.JobRequest;
import com.pbook.workflow.PbookE2EWorkflow;
import com.pbook.workflow.api.ExperimenterRunner;
import com.pbook.workflow.model.ExperimenterTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@Component
public class PbookScheduler {
    
    private static final Logger LOG = LoggerFactory.getLogger(PbookScheduler.class);
    
    private PbookE2EWorkflow pbookE2EWorkflow;
    private ExperimenterRunner pbookExperimenterRunner;
    private ThreadPoolTaskExecutor pbookTasker;
    
    @Autowired
    public PbookScheduler (PbookE2EWorkflow pbookE2EWorkflow, 
            ExperimenterRunner pbookExperimenterRunner, 
            ThreadPoolTaskExecutor pbookTasker) {
        this.pbookE2EWorkflow = pbookE2EWorkflow;
        this.pbookExperimenterRunner = pbookExperimenterRunner;
        this.pbookTasker = pbookTasker;
    }
    
    public void schedulePbookJob(final JobRequest request) {
        this.pbookTasker.execute(new Runnable() {
            @Override
            public void run() {
                try {
//                    pbookE2EWorkflow.execute(request.getFileUrls());
                    pbookExperimenterRunner.execute(new ExperimenterTrigger(PbookE2EWorkflow.PBOOK_ENTRIES_TABLE_NAME));
                } catch(Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
