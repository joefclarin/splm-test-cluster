package com.pbook.fixtures;

import com.pbook.rest.helpers.PhoneBookEntry;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Cromwell.Ellamil
 */
public class MockedJdbcTemplate extends JdbcTemplate {
    
	private final String TABLE_NAME = "pbook_entries_wod";
	
	// columns
	private final String FIRST_NAME = "firstname";
	private final String LAST_NAME  = "lastname";
	private final String ID         = "id";
	private final String NUMBER     = "number";
	private final String EMAIL      = "email";
	
	/* (non-Javadoc)
	 * @see org.springframework.jdbc.support.JdbcAccessor#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() {
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.jdbc.core.JdbcTemplate#query(java.lang.String, org.springframework.jdbc.core.RowMapper)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> query(String sql, RowMapper<T> rowMapper)
			throws DataAccessException {
            System.out.println(sql);
            
            if (sql.equals("SELECT * FROM " + TABLE_NAME)) {
                List<T> result = new ArrayList<>();
                result.add((T) new PhoneBookEntry(
                        "Rachelle Pauline", "Bulatao", "381", "+639173068531", "rachelle.pauline@nokia.com"));
                result.add((T) new PhoneBookEntry(
                        "Rafael Domingo", "Dela Cruz", "382", "+639173068565", "rafael_domingo.dela_cruz@nokia.com"));
                result.add((T) new PhoneBookEntry(
                        "Suzette", "Wong", "436", "+639173068702", "suzette.wong@nokia.com"));
                return result;
            }
            else if (sql.equals("SELECT * FROM " + TABLE_NAME + createWhereClause("ra"))) {
                List<T> result = new ArrayList<>();
                result.add((T) new PhoneBookEntry(
                        "Rachelle Pauline", "Bulatao", "381", "+639173068531", "rachelle.pauline@nokia.com"));
                result.add((T) new PhoneBookEntry(
                        "Rafael Domingo", "Dela Cruz", "382", "+639173068565", "rafael_domingo.dela_cruz@nokia.com"));
                return result;
            }
            else if (sql.equals("SELECT * FROM " + TABLE_NAME + createWhereClause("a very unique string that no entry matches"))) {
                List<T> result = new ArrayList<>();
                return result;
            }
            return null;
	}
        
        private String createWhereClause(String filter) {
            String where = " WHERE ";
                   where += FIRST_NAME + " = '" + filter + "' OR ";
                   where += LAST_NAME  + " = '" + filter + "' OR ";
                   where += ID         + " = '" + filter + "' OR ";
                   where += NUMBER     + " = '" + filter + "' OR ";
                   where += EMAIL      + " = '" + filter + "'";
            return where;
        }
}
