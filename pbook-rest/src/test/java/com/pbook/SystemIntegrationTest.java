package com.pbook;

import com.pbook.rest.fixtures.JettyStarter;
import com.pbook.rest.helpers.PhoneBookEntry;
import java.io.IOException;
import java.util.List;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.SimpleType;
import org.codehaus.jackson.type.JavaType;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;


/**
 *
 * @author Cromwell.Ellamil
 */
public class SystemIntegrationTest {
    
    private static JettyStarter server;
    
    @BeforeClass
    public static void setupClass() throws Exception {
        server = new JettyStarter();
        server.start();
    }
    
    @AfterClass
    public static void teardownClass() throws Exception {
        server.stop();
    }
    
    private DefaultHttpClient client;
    
    @Before
    public void setup() {
        client = new DefaultHttpClient();
    }
    
    @Test
    public void should_get_all_phonebook_entries() throws Exception {
        // When
        HttpGet phoneBookRequest = new HttpGet(server.getURL() + "/api/entries");
        HttpResponse phoneBookResponse = client.execute(phoneBookRequest);
        int code1 = phoneBookResponse.getStatusLine().getStatusCode();
        
        // Then
        assertThat(code1, is(equalTo(200)));
        
        // When
        List<PhoneBookEntry> phoneBookEntries = readBody(phoneBookResponse, CollectionType.construct(List.class,
                                                        SimpleType.construct(PhoneBookEntry.class)));
        
        // Then
        assertThat(phoneBookEntries.size(), is(equalTo(3)));
        assertTrue(phoneBookEntries.get(0).equals(new PhoneBookEntry(
                "Rachelle Pauline", "Bulatao", "381", "+639173068531", "rachelle.pauline@nokia.com")));
        assertTrue(phoneBookEntries.get(1).equals(new PhoneBookEntry(
                "Rafael Domingo", "Dela Cruz", "382", "+639173068565", "rafael_domingo.dela_cruz@nokia.com")));
        assertTrue(phoneBookEntries.get(2).equals(new PhoneBookEntry(
                "Suzette", "Wong", "436", "+639173068702", "suzette.wong@nokia.com")));
    }
    
    @Test
    public void should_get_all_phonebook_entries_with_given_filter() throws IOException {
        // Given
        String filterString = "ra";
        
        // When
        HttpGet phoneBookRequest = new HttpGet(server.getURL() + "/api/entries?filter=" + filterString);
        HttpResponse phoneBookResponse = client.execute(phoneBookRequest);
        int code1 = phoneBookResponse.getStatusLine().getStatusCode();
        
        // Then
        assertThat(code1, is(equalTo(200)));
        
        // When
        List<PhoneBookEntry> phoneBookEntries = readBody(phoneBookResponse, CollectionType.construct(List.class,
                                                        SimpleType.construct(PhoneBookEntry.class)));
        
        // Then
        assertThat(phoneBookEntries.size(), is(equalTo(2)));
        assertTrue(phoneBookEntries.get(0).equals(new PhoneBookEntry(
                "Rachelle Pauline", "Bulatao", "381", "+639173068531", "rachelle.pauline@nokia.com")));
        assertTrue(phoneBookEntries.get(1).equals(new PhoneBookEntry(
                "Rafael Domingo", "Dela Cruz", "382", "+639173068565", "rafael_domingo.dela_cruz@nokia.com")));
    }
    
    @Test
    public void should_return_empty_list_if_no_phonebook_entries_retrieved() throws IOException {
        // Given
        String filterString = "a very unique string that no entry matches";
        String encodedFilter = URIUtil.encodeQuery(filterString);
        
        // When
        HttpGet phoneBookRequest = new HttpGet(server.getURL() + "/api/entries?filter=" + encodedFilter);
        HttpResponse phoneBookResponse = client.execute(phoneBookRequest);
        int code1 = phoneBookResponse.getStatusLine().getStatusCode();
        
        // Then
        assertThat(code1, is(equalTo(200)));
        
        // When
        List<PhoneBookEntry> phoneBookEntries = readBody(phoneBookResponse, CollectionType.construct(List.class,
                                                        SimpleType.construct(PhoneBookEntry.class)));
        
        // Then
        assertThat(phoneBookEntries.size(), is(equalTo(0)));
    }

    private <T> T readBody(HttpResponse response, JavaType type) 
            throws IOException, JsonParseException, JsonMappingException {
        String responseBody = EntityUtils.toString(response.getEntity());
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(responseBody, type);
    }
}
