package com.pbook.rest;

import java.util.ArrayList;
import java.util.List;

import com.pbook.rest.helpers.JobRequest;
import com.pbook.workflow.PbookE2EWorkflow;
import com.pbook.workflow.api.ExperimenterRunner;
import com.pbook.workflow.model.ExperimenterTrigger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 *
 * @author Joefrenette.Clarin
 */
@RunWith(MockitoJUnitRunner.class)
public class PbookSchedulerTest {
    
    private PbookScheduler testObj;
    
    @Mock
    private PbookE2EWorkflow pbookE2EWorkflow;
    
    @Mock
    private ExperimenterRunner pbookExperimenterRunner;
    
    @Mock
    private ThreadPoolTaskExecutor pbookTasker;
    
    @Captor
    private ArgumentCaptor<Runnable> runnable;
    
    @Captor
    private ArgumentCaptor<List<String>> fileUrl;
    
    @Captor
    private ArgumentCaptor<ExperimenterTrigger> trigger;
    
    private static final String USER = "root";
    private static final String PASSWORD = "";
    
    @Test
    public void should_process_files_in_arraylist_by_invoking_workflow_and_experimenter() {
        // Given
        List<String> fileList = new ArrayList<>();
        fileList.add("/path/one");
        fileList.add("/path/two");
        fileList.add("/path/three");
        
        // When
        testObj = new PbookScheduler(pbookE2EWorkflow, pbookExperimenterRunner, pbookTasker);
        testObj.schedulePbookJob(JobRequest.aJobRequest(USER, PASSWORD, fileList));
        
        // Then
        verify(pbookTasker, times(1)).execute(runnable.capture());
        
        // When
        runnable.getValue().run();
        
        // Then
//        verify(pbookE2EWorkflow, times(1)).execute(fileUrl.capture());
//        assertThat(fileUrl.getValue(), equalTo(fileList));
        
        verify(pbookExperimenterRunner, times(1)).execute(trigger.capture());
        assertThat(trigger.getValue(), equalTo(new ExperimenterTrigger(PbookE2EWorkflow.PBOOK_ENTRIES_TABLE_NAME)));
    }
    
}
