package com.pbook.rest.helpers;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import static org.junit.Assert.assertThat;

/**
 *
 * @author Joefrenette.Clarin
 */
@RunWith(MockitoJUnitRunner.class)
public class PhoneBookRepositoryTest {
    
    private PhoneBookRepository testObj;
    
    private final String TABLE_NAME = "pbook_entries_wod";
    
    // columns
    private final String FIRST_NAME = "firstname";
    private final String LAST_NAME  = "lastname";
    private final String ID         = "id";
    private final String NUMBER     = "number";
    private final String EMAIL      = "email";
    
    @Mock
    JdbcTemplate presto;
    
    @Captor
    ArgumentCaptor<String> queryArgument;
    
    public PhoneBookRepositoryTest() {
    }
    
    @Before
    public void setUp() {
        testObj = new PhoneBookRepository(presto);
    }
    
    @Test
    public void should_create_query_without_parameters() {
        String correctQuery = "SELECT * FROM " + TABLE_NAME;
        
        // WHEN
        testObj.getEntries();
        
        // THEN
        verify(presto).query(queryArgument.capture(), any(ColumnMapRowMapper.class));
        String query = queryArgument.getValue();
        query = query.trim().replace("  ", " "); // remove double spaces due to concatenation
        
        assertThat(query, CoreMatchers.equalTo(correctQuery));
    }
    
    @Test
    public void should_create_query_with_parameters() {
        String filter = "aaa";
        String correctQuery  = "SELECT * FROM " + TABLE_NAME;
        
        if(filter != null && !filter.isEmpty()) {
            correctQuery += " WHERE ";
            correctQuery += FIRST_NAME + " = '" + filter + "' OR ";
            correctQuery += LAST_NAME  + " = '" + filter + "' OR ";
            correctQuery += ID         + " = '" + filter + "' OR ";
            correctQuery += NUMBER     + " = '" + filter + "' OR ";
            correctQuery += EMAIL      + " = '" + filter + "'";
        }
        
        // WHEN
        testObj.getEntries(filter);
        
        // THEN
        verify(presto).query(queryArgument.capture(), any(ColumnMapRowMapper.class));
        String query = queryArgument.getValue();
        query = query.trim().replace("  ", " "); // remove double spaces due to concatenation
        
        assertThat(query, CoreMatchers.equalTo(correctQuery));
    }
    
}
