package com.pbook.rest;

import com.pbook.rest.helpers.JobRequest;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.scheduling.TaskScheduler;

/**
 *
 * @author Cromwell.Ellamil
 */
@RunWith(MockitoJUnitRunner.class)
public class PbookFtpWatcherTest {
    
    @Mock
    private PbookScheduler scheduler;
    
    @Mock
    private TaskScheduler executor;
    
    @Captor
    ArgumentCaptor<Runnable> runnable;
    
    @Captor
    ArgumentCaptor<JobRequest> jobRequest;
    
    @Captor
    ArgumentCaptor<Long> rate;
    
    @Captor
    ArgumentCaptor<List<String>> filesProcessed;
    
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();
    
    private PbookFtpWatcher testFtpWatcher;
    private String ftpRoot;
    private static final long FIVE_MINUTES = 300000L;
    private static final String USER = "root";
    private static final String PASSWORD = "";
    
    public PbookFtpWatcherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws IOException {
        ftpRoot = folder.getRoot().getAbsolutePath() + "\\";
        folder.newFolder(PbookFtpWatcher.INBOX);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void should_list_all_files_with_valid_pattern_in_inbox_upon_initialization() 
            throws IOException {
        
        // Given
        String firstFile = PbookFtpWatcher.INBOX + "/pbook-001.tsv";
        String secondFile = PbookFtpWatcher.INBOX + "/pbook-002.tsv";
        String thirdFile = PbookFtpWatcher.INBOX + "/pbook-003.tsv";
        createFile(ftpRoot + firstFile);
        createFile(ftpRoot + secondFile);
        createFile(ftpRoot + thirdFile);
        
        // When
        testFtpWatcher = new PbookFtpWatcher(scheduler, executor, ftpRoot, USER, PASSWORD);
        verify(executor, times(1)).scheduleAtFixedRate(runnable.capture(), rate.capture());
        runnable.getAllValues().get(0).run();
        verify(scheduler, times(1)).schedulePbookJob(jobRequest.capture());
        JobRequest request = jobRequest.getValue();

        // Then
        assertThat(rate.getValue(), equalTo(FIVE_MINUTES));
        assertThat(request.getUsername(), equalTo(USER));
        assertThat(request.getPassword(), equalTo(PASSWORD));
        assertThat(request.getFileUrls(), equalTo(Arrays.asList(
                "file://" + PbookFtpWatcher.FTP_ROOT_JETTY + firstFile,
                "file://" + PbookFtpWatcher.FTP_ROOT_JETTY + secondFile,
                "file://" + PbookFtpWatcher.FTP_ROOT_JETTY + thirdFile)));
    }
    
    @Test
    public void should_not_list_files_with_invalid_pattern_in_inbox_upon_initialization() 
            throws IOException {
        
        // Given
        String firstFile = PbookFtpWatcher.INBOX + "/pb00k-004.tsv";
        String secondFile = PbookFtpWatcher.INBOX + "/pb00k-005.tsv";
        String thirdFile = PbookFtpWatcher.INBOX + "/pbook-006.tsv";
        createFile(ftpRoot + firstFile);
        createFile(ftpRoot + secondFile);
        createFile(ftpRoot + thirdFile);
        
        // When
        testFtpWatcher = new PbookFtpWatcher(scheduler, executor, ftpRoot, USER, PASSWORD);
        verify(executor, times(1)).scheduleAtFixedRate(runnable.capture(), rate.capture());
        runnable.getAllValues().get(0).run();
        verify(scheduler, times(1)).schedulePbookJob(jobRequest.capture());
        JobRequest request = jobRequest.getValue();
        
        // Then
        assertThat(rate.getValue(), equalTo(FIVE_MINUTES));
        assertThat(request.getUsername(), equalTo(USER));
        assertThat(request.getPassword(), equalTo(PASSWORD));
        assertThat(request.getFileUrls(), equalTo(Arrays.asList(
                "file://" + PbookFtpWatcher.FTP_ROOT_JETTY + thirdFile)));
    }
    
    private void createFile(String path) throws IOException {
        File f = new File(path);
        f.createNewFile();
    }
    
}
